(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["chat-chat-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/chat/chat.page.html":
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/chat/chat.page.html ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content class=\"ion-padding custom-background-color\">\n  <ion-toolbar class=\"chat-title\">\n    <ion-title class=\"ion-text-center\">Nome Usuario</ion-title>\n  </ion-toolbar>\n  <div class=\"chat-content\">\n\n\n    <ion-row class=\"chat-rigth\">\n      <ion-col>\n        <ion-card class=\"ion-float-left\">\n          <ion-card-content>\n            Olá, Bora pro play?\n          </ion-card-content>\n          <div class=\"chat-date\">20:00</div>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n\n\n    <ion-row class=\"chat-left\">\n      <ion-col>\n        <ion-card class=\"ion-float-right\">\n          <ion-card-content>\n            Vamos sim, eu só vou escrever uma frase grande para ver como que fica no card!\n          </ion-card-content>\n          <div class=\"chat-date\">20:00</div>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n\n\n    <ion-row class=\"chat-left\">\n      <ion-col >\n        <ion-card class=\"ion-float-right\">\n          <ion-card-content>\n            Vamos sim, eu só vou escrever uma frase grande para ver como que fica no card!\n          </ion-card-content>\n          <div class=\"chat-date\">20:00</div>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n\n    <ion-row class=\"chat-rigth\">\n      <ion-col>\n        <ion-card class=\"ion-float-left\">\n          <ion-card-content>\n            Olá, Bora pro play?\n          </ion-card-content>\n          <div class=\"chat-date\">20:00</div>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n\n    <ion-row class=\"chat-left\">\n      <ion-col>\n        <ion-card class=\"ion-float-right\">\n          <ion-card-content>\n            Olá, Bora pro play?\n          </ion-card-content>\n          <div class=\"chat-date\">20:00</div>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n\n  </div>\n\n  <ion-row class=\"input-message\">\n    <ion-input placeholder=\"Your Message\"></ion-input>\n    <ion-button>Send</ion-button>\n  </ion-row>\n</ion-content>");

/***/ }),

/***/ "./src/app/chat/chat-routing.module.ts":
/*!*********************************************!*\
  !*** ./src/app/chat/chat-routing.module.ts ***!
  \*********************************************/
/*! exports provided: ChatPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatPageRoutingModule", function() { return ChatPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _chat_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./chat.page */ "./src/app/chat/chat.page.ts");




const routes = [
    {
        path: '',
        component: _chat_page__WEBPACK_IMPORTED_MODULE_3__["ChatPage"]
    }
];
let ChatPageRoutingModule = class ChatPageRoutingModule {
};
ChatPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ChatPageRoutingModule);



/***/ }),

/***/ "./src/app/chat/chat.module.ts":
/*!*************************************!*\
  !*** ./src/app/chat/chat.module.ts ***!
  \*************************************/
/*! exports provided: ChatPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatPageModule", function() { return ChatPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _chat_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./chat-routing.module */ "./src/app/chat/chat-routing.module.ts");
/* harmony import */ var _chat_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./chat.page */ "./src/app/chat/chat.page.ts");







let ChatPageModule = class ChatPageModule {
};
ChatPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _chat_routing_module__WEBPACK_IMPORTED_MODULE_5__["ChatPageRoutingModule"]
        ],
        declarations: [_chat_page__WEBPACK_IMPORTED_MODULE_6__["ChatPage"]]
    })
], ChatPageModule);



/***/ }),

/***/ "./src/app/chat/chat.page.scss":
/*!*************************************!*\
  !*** ./src/app/chat/chat.page.scss ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".input-message {\n  background-color: white;\n  color: black;\n  width: 100%;\n  margin: 0;\n  position: fixed;\n  left: 0;\n  bottom: 0;\n}\n\n.chat-left ion-card {\n  background-color: #2d68d4;\n}\n\n.chat-left ion-card ion-card-content {\n  text-align: right;\n  color: white;\n}\n\n.chat-left ion-card .chat-date {\n  color: rgba(255, 255, 255, 0.8);\n  margin-bottom: 5px;\n  margin-left: 5px;\n  float: left;\n  background-color: #2d68d4;\n}\n\n.chat-rigth .chat-date {\n  color: rgba(0, 0, 0, 0.4);\n  margin-bottom: 5px;\n  margin-right: 5px;\n  float: right;\n}\n\n.chat-title {\n  --background: #702800;\n  color: white;\n  top: 0;\n  left: 0;\n  margin: 0;\n  width: 100%;\n  position: fixed;\n  border-color: white;\n}\n\n.chat-content {\n  margin-top: 48px;\n  margin-bottom: 28px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2RhdmkvUHJvamVjdHMvZHVuZ2Vvbm1hdGNoL2R1bmdlb24tbWF0Y2gvc3JjL2FwcC9jaGF0L2NoYXQucGFnZS5zY3NzIiwic3JjL2FwcC9jaGF0L2NoYXQucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksdUJBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLFNBQUE7RUFDQSxlQUFBO0VBQ0EsT0FBQTtFQUNBLFNBQUE7QUNDSjs7QURHSTtFQUNJLHlCQUFBO0FDQVI7O0FERVE7RUFDSSxpQkFBQTtFQUNBLFlBQUE7QUNBWjs7QURHUTtFQUNJLCtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtFQUNBLFdBQUE7RUFDQSx5QkFBQTtBQ0RaOztBRFFJO0VBQ0kseUJBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0EsWUFBQTtBQ0xSOztBRFVBO0VBQ0kscUJBQUE7RUFDQSxZQUFBO0VBQ0EsTUFBQTtFQUNBLE9BQUE7RUFDQSxTQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQSxtQkFBQTtBQ1BKOztBRFVBO0VBQ0ksZ0JBQUE7RUFDQSxtQkFBQTtBQ1BKIiwiZmlsZSI6InNyYy9hcHAvY2hhdC9jaGF0LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5pbnB1dC1tZXNzYWdle1xuICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xuICAgIGNvbG9yOiBibGFjaztcbiAgICB3aWR0aDogMTAwJTtcbiAgICBtYXJnaW46IDA7XG4gICAgcG9zaXRpb246IGZpeGVkO1xuICAgIGxlZnQ6IDA7XG4gICAgYm90dG9tOiAwO1xufVxuXG4uY2hhdC1sZWZ0eyAgICAgICAgXG4gICAgaW9uLWNhcmR7IFxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMmQ2OGQ0O1xuXG4gICAgICAgIGlvbi1jYXJkLWNvbnRlbnQge1xuICAgICAgICAgICAgdGV4dC1hbGlnbjogcmlnaHQ7XG4gICAgICAgICAgICBjb2xvcjogd2hpdGU7XG4gICAgICAgIH1cblxuICAgICAgICAuY2hhdC1kYXRlIHtcbiAgICAgICAgICAgIGNvbG9yOiByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuOCk7XG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiA1cHg7XG4gICAgICAgICAgICBtYXJnaW4tbGVmdDogNXB4O1xuICAgICAgICAgICAgZmxvYXQ6IGxlZnQ7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMmQ2OGQ0O1xuICAgICAgICAgICAgXG4gICAgICAgIH1cbiAgICB9XG59XG5cbi5jaGF0LXJpZ3Roe1xuICAgIC5jaGF0LWRhdGUge1xuICAgICAgICBjb2xvcjogcmdiYSgwLCAwLCAwLCAwLjQpO1xuICAgICAgICBtYXJnaW4tYm90dG9tOiA1cHg7XG4gICAgICAgIG1hcmdpbi1yaWdodDogNXB4O1xuICAgICAgICBmbG9hdDogcmlnaHQ7XG4gICAgICAgIFxuICAgIH1cbn1cblxuLmNoYXQtdGl0bGV7XG4gICAgLS1iYWNrZ3JvdW5kOiAjNzAyODAwO1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICB0b3A6IDA7XG4gICAgbGVmdDogMDtcbiAgICBtYXJnaW46IDA7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgcG9zaXRpb246IGZpeGVkO1xuICAgIGJvcmRlci1jb2xvcjogd2hpdGU7ICAgIFxufVxuXG4uY2hhdC1jb250ZW50e1xuICAgIG1hcmdpbi10b3A6IDQ4cHg7XG4gICAgbWFyZ2luLWJvdHRvbTogMjhweDtcbn0iLCIuaW5wdXQtbWVzc2FnZSB7XG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xuICBjb2xvcjogYmxhY2s7XG4gIHdpZHRoOiAxMDAlO1xuICBtYXJnaW46IDA7XG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgbGVmdDogMDtcbiAgYm90dG9tOiAwO1xufVxuXG4uY2hhdC1sZWZ0IGlvbi1jYXJkIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzJkNjhkNDtcbn1cbi5jaGF0LWxlZnQgaW9uLWNhcmQgaW9uLWNhcmQtY29udGVudCB7XG4gIHRleHQtYWxpZ246IHJpZ2h0O1xuICBjb2xvcjogd2hpdGU7XG59XG4uY2hhdC1sZWZ0IGlvbi1jYXJkIC5jaGF0LWRhdGUge1xuICBjb2xvcjogcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjgpO1xuICBtYXJnaW4tYm90dG9tOiA1cHg7XG4gIG1hcmdpbi1sZWZ0OiA1cHg7XG4gIGZsb2F0OiBsZWZ0O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMmQ2OGQ0O1xufVxuXG4uY2hhdC1yaWd0aCAuY2hhdC1kYXRlIHtcbiAgY29sb3I6IHJnYmEoMCwgMCwgMCwgMC40KTtcbiAgbWFyZ2luLWJvdHRvbTogNXB4O1xuICBtYXJnaW4tcmlnaHQ6IDVweDtcbiAgZmxvYXQ6IHJpZ2h0O1xufVxuXG4uY2hhdC10aXRsZSB7XG4gIC0tYmFja2dyb3VuZDogIzcwMjgwMDtcbiAgY29sb3I6IHdoaXRlO1xuICB0b3A6IDA7XG4gIGxlZnQ6IDA7XG4gIG1hcmdpbjogMDtcbiAgd2lkdGg6IDEwMCU7XG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgYm9yZGVyLWNvbG9yOiB3aGl0ZTtcbn1cblxuLmNoYXQtY29udGVudCB7XG4gIG1hcmdpbi10b3A6IDQ4cHg7XG4gIG1hcmdpbi1ib3R0b206IDI4cHg7XG59Il19 */");

/***/ }),

/***/ "./src/app/chat/chat.page.ts":
/*!***********************************!*\
  !*** ./src/app/chat/chat.page.ts ***!
  \***********************************/
/*! exports provided: ChatPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatPage", function() { return ChatPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


let ChatPage = class ChatPage {
    constructor() { }
    ngOnInit() {
    }
};
ChatPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-chat',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./chat.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/chat/chat.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./chat.page.scss */ "./src/app/chat/chat.page.scss")).default]
    }),
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [])
], ChatPage);



/***/ })

}]);
//# sourceMappingURL=chat-chat-module-es2015.js.map