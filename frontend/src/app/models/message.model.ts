export class Message {
    id: string;
    date: string;
    message: string;
    sender: string;
}
