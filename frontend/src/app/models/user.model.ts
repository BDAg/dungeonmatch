export class User {
    id: string;
    name: string;
    email: string;
    phone: string;
    user_type: 'player' | 'master' | 'both';
    avatar: string;
    location: any;
    rpg_systems: string[];
    created_at: Date;
    updated_at: Date;
}