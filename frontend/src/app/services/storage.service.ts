import { Injectable } from '@angular/core';
import { Plugins } from '@capacitor/core';
const { Storage } = Plugins;

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor() { }

  async setObject(key: string, value) {
    await Storage.set({ key, value: JSON.stringify(value) })
  }

  async getObject(key: string) {
    const { value } = await Storage.get({ key });
    return JSON.parse(value);
  }

  async setItem(key: string, value: any) {
    await Storage.set({ key, value })
  }

  async getItem(key: string) {
    const { value } = await Storage.get({ key });
    return value;
  }

  async removeItem(key: string) {
    await Storage.remove({ key });
  }

  async keys() {
    const { keys } = await Storage.keys();
    return keys;
  }

  async clear() {
    await Storage.clear();
  }
}
