import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../models/user.model';
import { AuthService } from './auth.service';
import { ActionSheetController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  apiUrl = environment.apiUrl;

  constructor(
    private http: HttpClient,
    private authService: AuthService
  ) { }

  getHeaders() {
    const token = this.authService.token();
    const headers = new HttpHeaders({
      Authorization: `Bearer ${token}`
    });
    return {
      headers
    };
  }
  
  getUserById(userId) {
    return this.http.get<User>(`${this.apiUrl}/users/${userId}`, this.getHeaders());
  }


  updateUser(form) {
    return this.http.put<User>(`${this.apiUrl}/users`, form, this.getHeaders());
  }

  uploadImage(formData: FormData) {
    return this.http.patch(`${this.apiUrl}/users/avatar`, formData, this.getHeaders()).toPromise();
  }

  getNearbyUsers(location, maxDistance = 10) {
    const httpOptions = {
      params: {
        latitude: location.latitude.toString(),
        longitude: location.longitude.toString(),
        maxDistance: maxDistance.toString()
      },
      ...this.getHeaders()
    }
    return this.http.get<User[]>(`${this.apiUrl}/users`, httpOptions);
  }
}
