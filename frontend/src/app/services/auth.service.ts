import { Injectable } from '@angular/core';
import { StorageService } from './storage.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { Platform } from '@ionic/angular';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  authState = new BehaviorSubject<User>(new User());
  tokenState = new BehaviorSubject<string>('');
  apiUrl = environment.apiUrl;

  constructor(
    private platform: Platform,
    private storage: StorageService,
    private http: HttpClient
  ) {
    this.platform.ready().then(() => {
      this.checkLogin();
    });
  }

  async checkLogin() {
    const user: User = await this.storage.getObject('user');
    const token: string = await this.storage.getItem('token');
    if (user && !!user.email && !!token) {
      this.authState.next(user);
      this.tokenState.next(token);
    } else {
      this.storage.clear();
    }
  }

  stateValue(): User {
    return this.authState.value;
  }

  async updateUserState(user: User) {
    await this.storage.setObject('user', user);
    this.authState.next(user);
  }

  token(): string {
    return this.tokenState.value;
  }

  state(): Observable<User> {
    return this.authState.asObservable();
  }

  login(form: { email: string, password: string }) {
    return this.http.post<{ user: User, token: string }>(`${this.apiUrl}/sessions/`, form);
  }

  register(form) {
    return this.http.post<{ user: User, token: string }>(`${this.apiUrl}/users/`, form);
  }

  isAuthenticated(): boolean {
    return !!this.authState.value.email;
  }

  async logout() {
    await this.storage.clear();
    this.authState.next(new User());
  }
}
