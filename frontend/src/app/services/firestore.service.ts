import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import * as firebase from 'firebase/app';
import { map, tap } from 'rxjs/operators';
import { Message } from 'src/app/models/message.model';

@Injectable({
  providedIn: 'root'
})
export class FirestoreService {

  constructor(public firestore: AngularFirestore) {

  }

  sendMessage(message, userId, userChatId) {

    const id = this.firestore.createId();
    const chatId = [userId, userChatId].sort().join('.');

    const message_data = {
      id: id,
      date: new Date(),
      message: message,
      sender: userId
    };

    this.firestore.collection('chats').doc(chatId).update({
      messages: firebase.firestore.FieldValue.arrayUnion(message_data)
    }).then(() => {
      return true;

    }).catch((error) => {
      this.firestore.collection('chats').doc(chatId).set({
        users: [userId, userChatId],
        messages: firebase.firestore.FieldValue.arrayUnion(message_data)
      }).then(() => {
        return true;
      });
    });

  }

  getMessages(userId, userChatId) {
    const chatId = [userId, userChatId].sort().join('.');
    return this.firestore.collection('chats').doc(chatId).snapshotChanges()
      .pipe(
        map((changes: any) => {
          const data = changes.payload.data();
          if (!data)
            return [];
          
          return ('messages' in data) ? data['messages'] as Message[] : [];
        }));
  }

  listChats(userId) {
    return this.firestore.collection('chats').ref.where('users', 'array-contains', userId).get();
  }
}
