import { Injectable } from '@angular/core';

interface RPGSystems {
  name: string;
  value: string;
}

@Injectable({
  providedIn: 'root'
})
export class ConstantsService {
  public readonly rpgSystems: RPGSystems[] = [
    {
      name: 'DND',
      value: 'dnd'
    },
    {
      name: 'Chutullo',
      value: 'chutullo'
    },
  ];
  constructor() { }
}
