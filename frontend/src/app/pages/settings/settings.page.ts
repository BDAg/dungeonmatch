import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User } from 'src/app/models/user.model';
import { Plugins } from '@capacitor/core';
import { ConstantsService } from 'src/app/services/constants.service';
import { Subscription } from 'rxjs';
import { UsersService } from 'src/app/services/users.service';
import { ToastController } from '@ionic/angular';

const { Geolocation } = Plugins;

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit, OnDestroy {
  subscription = new Subscription();
  form: FormGroup;
  user: User;
  rpgSystems: any[];
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private toastController: ToastController,
    private authService: AuthService,
    private usersService: UsersService,
    private constantsService: ConstantsService,
  ) { }

  ngOnInit() {
    this.rpgSystems = this.constantsService.rpgSystems;
    this.form = this.formBuilder.group({
      name: [null, Validators.compose([Validators.required, Validators.minLength(5)])],
      email: [null, Validators.required],
      phone: [null, Validators.compose([Validators.required, Validators.minLength(11)])],
      location: [null, Validators.required],
      user_type: [null, Validators.compose([Validators.required, Validators.minLength(1)])],
      avatar: [null, Validators.required],
      rpg_systems: [[], Validators.required]
    });

    this.subscription.add(
      this.authService.state().subscribe(user => {
        this.user = user;
        this.form.get('name').setValue(this.user.name);
        this.form.get('email').setValue(this.user.email);
        this.form.get('phone').setValue(this.user.phone);
        this.form.get('location').setValue(this.user.location);
        this.form.get('user_type').setValue(this.user.user_type);
        this.form.get('avatar').setValue(this.user.avatar);
        this.form.get('rpg_systems').setValue(this.user.rpg_systems);
      })
    );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  updateUserAvatar(avatar: string) {
    let user;
    if (this.form.valid) {
      user = this.form.value;
    } else {
      user = { ...this.user }
    }
    user.avatar = avatar;
    if (typeof (user.location) !== 'string') {
      user.location = `${user.location.x}, ${user.location.y}`
    }

    this.usersService.updateUser(user)
      .subscribe(async _user => {
        this.authService.updateUserState(_user);
        const toast = await this.toastController.create({
          message: 'Profile Updated',
          duration: 2000
        });
        toast.present();
      }, async error => {
        console.error(error);
        const toast = await this.toastController.create({
          message: 'Error',
          duration: 2000
        });
        toast.present();
      });
  }

  async getLocation() {
    try {
      await Geolocation.requestPermissions();
      const position = await Geolocation.getCurrentPosition();
      this.form.get('location').setValue(
        `${position.coords.latitude.toString()}, ${position.coords.longitude.toString()}`
      );
    } catch (e) {
      console.error('not allowed', e);
    }
  }

  async updateUser() {
    this.submitted = true;
    if (this.form.invalid) {
      this.form.markAllAsTouched();
      return;
    }

    await this.getLocation();

    this.usersService.updateUser(this.form.value)
      .subscribe(async _user => {
        this.authService.updateUserState(_user);
        const toast = await this.toastController.create({
          message: 'Profile Updated',
          duration: 2000
        });
        toast.present();
      }, async error => {
        console.error(error);
        const toast = await this.toastController.create({
          message: 'Error',
          duration: 2000
        });
        toast.present();
      });
  }

  async logout() {
    await this.authService.logout();
  }

}
