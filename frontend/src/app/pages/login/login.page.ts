import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { StorageService } from '../../services/storage.service';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  form: FormGroup;
  invalidUser = false;
  error = false;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private storage: StorageService,
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      email: [null, Validators.compose([Validators.required, Validators.email])],
      password: [null, Validators.compose([Validators.required, Validators.minLength(6)])]
    });
  }

  signIn() {
    this.submitted = true;
    if (this.form.invalid) {
      this.form.markAllAsTouched();
      return;
    }

    this.invalidUser = false;
    this.error = false;

    this.authService.login(this.form.value).toPromise()
      .then(async res => {
        const { user, token } = res;
        await this.storage.setItem('token', token);
        await this.storage.setObject('user', user);
        this.authService.tokenState.next(token);
        this.authService.authState.next(user);
      }).catch(e => {
        if (e.status === 401) {
          this.invalidUser = true;
        } else {
          this.error = true;
        }
        console.error(e);
      });
  }
}
