import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { User } from 'src/app/models/user.model';
import { ConstantsService } from 'src/app/services/constants.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  user: User;
  rpgSystems: any[];

  constructor(
    private authService: AuthService,
    private constantsService: ConstantsService
  ) { }

  ngOnInit() {
    this.rpgSystems = this.constantsService.rpgSystems;
    this.user = this.authService.stateValue();
  }

  get userRpgSystems() {
    return this.rpgSystems.filter(rpgSystem => this.user.rpg_systems.includes(rpgSystem.value));
  }
}
