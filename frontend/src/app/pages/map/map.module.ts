import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MapPageRoutingModule } from './map-routing.module';

import { MapPage } from './map.page';
import { ModalProfilePage } from '../modal-profile/modal-profile.page';
import { ModalProfilePageModule } from '../modal-profile/modal-profile.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MapPageRoutingModule,
    ModalProfilePageModule
  ],
  declarations: [MapPage]
})
export class MapPageModule {}
