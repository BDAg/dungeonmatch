import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ModalProfilePage } from '../modal-profile/modal-profile.page';
import { AuthService } from 'src/app/services/auth.service';
import { User } from 'src/app/models/user.model';
import { Plugins } from '@capacitor/core';
import { UsersService } from 'src/app/services/users.service';
import { environment } from 'src/environments/environment';
import mapboxgl from 'mapbox-gl'

const { Geolocation } = Plugins;

@Component({
  selector: 'app-map',
  templateUrl: './map.page.html',
  styleUrls: ['./map.page.scss'],
})
export class MapPage implements OnInit {
  @ViewChild('map') _map: ElementRef;
  user: User;
  users: User[] = [];
  map: any;
  filter = 'both';
  currentMarkers = [];
  location;

  constructor(
    public modalController: ModalController,
    public authService: AuthService,
    private usersService: UsersService,
  ) { }

  async ngOnInit(
  ) {
    this.initMap();
  }

  async initMap() {
    this.user = this.authService.stateValue();
    this.location = await this.getLocation();
    mapboxgl.accessToken = environment.mapbox.accessToken;
    this.map = new mapboxgl.Map({
      container: this._map.nativeElement,
      style: 'mapbox://styles/mapbox/streets-v11?optimize=true',
      center: [this.location.longitude, this.location.latitude], // starting position [lng, lat]
      zoom: 14,
      minZoom: 13,
      maxZoom: 15
    });

    await this.findUsers();
  }

  async findUsers() {
    this.usersService.getNearbyUsers(this.location)
      .subscribe(users => {
        this.users = users;

        this.filterUsers();
      }, error => {
        console.error(error);
      })
  }

  filterUsers() {
    this.currentMarkers.forEach(m => {
      m.remove();
    });

    this.currentMarkers = [];

    this.users
      .filter(user => this.filter === 'both' || this.filter === user.user_type)
      .forEach(user => {
        const popup = new mapboxgl.Popup();
        const m = new mapboxgl.Marker()
          .setLngLat([user.location.y, user.location.x])
          .setPopup(popup)
          .addTo(this.map);

        this.currentMarkers.push(m);

        popup.on('open', () => {
          this.showProfile(user);
        })
      });
  }

  async showProfile(_selectedUser: User) {
    const selectedUser = this.users.filter(_user => _user.id === _selectedUser.id);
    if (!selectedUser.length) {
      return;
    }
    const user = selectedUser[0];
    const modal = await this.modalController.create({
      component: ModalProfilePage,
      componentProps: {
        user
      }
    });
    return await modal.present();
  }

  async getLocation() {
    try {
      await Geolocation.requestPermissions();
      const position = await Geolocation.getCurrentPosition();
      return position.coords;
    } catch (e) {
      // Show toast
      console.log('not allowed', e);
      return;
    }
  }
}
