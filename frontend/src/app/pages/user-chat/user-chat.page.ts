import { Component, OnInit, OnDestroy } from '@angular/core';
import { User } from 'src/app/models/user.model';
import { Message } from 'src/app/models/message.model';
import { AuthService } from 'src/app/services/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { FirestoreService } from '../../services/firestore.service';


@Component({
  selector: 'app-user-chat',
  templateUrl: './user-chat.page.html',
  styleUrls: ['./user-chat.page.scss'],
})
export class UserChatPage implements OnInit, OnDestroy {
  subscription = new Subscription();
  user: User;
  userChat: User;
  messages: Message[];
  message = '';

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authService: AuthService,
    private firestoreService: FirestoreService
  ) { }

  ngOnInit() {
    this.user = this.authService.stateValue();
    this.subscription.add(
      this.route.queryParams.subscribe(params => {
        if (this.router.getCurrentNavigation().extras.state) {
          this.userChat = this.router.getCurrentNavigation().extras.state.user;
          this.getUserMessages();
        }
      })
    );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  sendMessage() {
    this.message = this.message.trim();
    if (!this.message.length) {
      return
    }
    this.firestoreService.sendMessage(this.message, this.user.id, this.userChat.id);
    this.message = '';
  }

  getUserMessages() {
    this.firestoreService.getMessages(this.user.id, this.userChat.id).subscribe((data:Message[]) => {
      this.messages = data;
    });
  }

}
