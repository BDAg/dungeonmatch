import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UserChatPage } from './user-chat.page';

describe('UserChatPage', () => {
  let component: UserChatPage;
  let fixture: ComponentFixture<UserChatPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserChatPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(UserChatPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
