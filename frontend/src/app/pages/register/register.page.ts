import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Plugins } from '@capacitor/core';
import { AuthService } from 'src/app/services/auth.service';
import { StorageService } from 'src/app/services/storage.service';
import { ConstantsService } from 'src/app/services/constants.service';

const { Geolocation } = Plugins;

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  form: FormGroup;
  rpgSystems: any[];
  submitted = false;
  emailInvalid = false;
  error = false;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private storageService: StorageService,
    private constantsService: ConstantsService
  ) { }

  ngOnInit() {
    this.rpgSystems = this.constantsService.rpgSystems;
    this.form = this.formBuilder.group({
      name: [null, Validators.compose([Validators.required, Validators.minLength(5)])],
      email: [null, Validators.compose([Validators.required, Validators.email])],
      password: [null, Validators.compose([Validators.required, Validators.minLength(6)])],
      phone: [null, Validators.compose([Validators.required, Validators.minLength(11)])],
      user_type: [null, Validators.compose([Validators.required, Validators.minLength(1)])],
      avatar: ['assets/img/default-avatar.webp', Validators.required],
      location: [null, Validators.required],
      rpg_systems: [[], Validators.required]
    });

    this.getLocation();
  }

  async getLocation(submitAgain=false) {
    try {
      await Geolocation.requestPermissions();
      const position = await Geolocation.getCurrentPosition();
      this.form.get('location').setValue(
        `${position.coords.latitude.toString()}, ${position.coords.longitude.toString()}`
      );
      if (submitAgain) {
        this.signUp();
      }
    } catch (e) {
      // Show toast
      console.log('not allowed', e);
    }
  }

  async signUp() {
    this.submitted = true;
    if (this.form.invalid) {
      this.form.markAllAsTouched();
      if (this.form.get('location').invalid) {
        this.getLocation(true);
      }
      return;
    }

    this.emailInvalid = false;
    this.error = false;
    this.authService.register(this.form.value).toPromise()
      .then(async res => {
        const { user, token } = res;
        await this.storageService.setItem('token', token);
        await this.storageService.setObject('user', user);
        this.authService.tokenState.next(token);
        this.authService.authState.next(user);
      }).catch(e => {
        if (e.status === 400) {
          this.emailInvalid = true;
        } else {
          this.error = true;
        }
        console.error(e);
      });
  }

}
