import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ConstantsService } from 'src/app/services/constants.service';
import { User } from 'src/app/models/user.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-modal-profile',
  templateUrl: './modal-profile.page.html',
  styleUrls: ['./modal-profile.page.scss'],
})
export class ModalProfilePage implements OnInit {
  @Input() user: User;
  rpgSystems: any[];

  constructor(
    private router: Router,
    private modalController: ModalController,
    private constantsService: ConstantsService,
  ) { }

  ngOnInit() {
    this.rpgSystems = this.constantsService.rpgSystems;
  }

  close() {
    this.modalController.dismiss();
  }

  get userRpgSystems() {
    return this.rpgSystems.filter(rpgSystem => this.user.rpg_systems.includes(rpgSystem.value));
  }

  contact() {
    this.router.navigate(['user-chat'], {
      state: {
        user: this.user
      }
    });
    this.close();
  }

}
