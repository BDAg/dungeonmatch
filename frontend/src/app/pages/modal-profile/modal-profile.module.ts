import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ModalProfilePageRoutingModule } from './modal-profile-routing.module';

import { ModalProfilePage } from './modal-profile.page';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ModalProfilePageRoutingModule,
    ComponentsModule
  ],
  declarations: [ModalProfilePage],
  entryComponents: [ModalProfilePage]
})
export class ModalProfilePageModule { }
