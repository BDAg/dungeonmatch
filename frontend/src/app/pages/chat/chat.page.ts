import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { User } from 'src/app/models/user.model';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { FirestoreService } from '../../services/firestore.service';
import { UsersService } from 'src/app/services/users.service';


@Component({
  selector: 'app-chat',
  templateUrl: './chat.page.html',
  styleUrls: ['./chat.page.scss'],
})
export class ChatPage implements OnInit, OnDestroy {
  subscription = new Subscription();
  user: User;
  users: User[] = [];

  constructor(
    private router: Router,
    private authService: AuthService,
    private usersService: UsersService,
    private firestoreService:FirestoreService
  ) { }

  ngOnInit() {
    this.user = this.authService.stateValue();
    this.firestoreService.listChats(this.user.id).then(data => {
      data.docs.map(doc => {
        var users = doc.data()['users'] as string[];
        var index = users.indexOf(this.user.id);
        if (index !== -1) users.splice(index, 1);

        this.usersService.getUserById(users[0]).subscribe((user:User) => {
          this.users.push(user);
        });
      })
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  openChat(user) {
    this.router.navigate(['user-chat'], {
      state: {
        user
      }
    });
  }

}
