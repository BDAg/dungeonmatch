import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileAvatarComponent } from './profile-avatar/profile-avatar.component';
import { IonicModule } from '@ionic/angular';
import { FormErrorMessageComponent } from './form-error-message/form-error-message.component';



@NgModule({
  declarations: [
    ProfileAvatarComponent,
    FormErrorMessageComponent
  ],
  imports: [
    CommonModule,
    IonicModule
  ],
  exports: [
    ProfileAvatarComponent,
    FormErrorMessageComponent
  ]
})
export class ComponentsModule { }
