import { Component, OnInit, Input } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-form-error-message',
  templateUrl: './form-error-message.component.html',
  styleUrls: ['./form-error-message.component.scss'],
})
export class FormErrorMessageComponent implements OnInit {
  @Input() control: FormControl;
  @Input() controlName: string;
  @Input() error: string;

  constructor() { }

  ngOnInit() { }

}
