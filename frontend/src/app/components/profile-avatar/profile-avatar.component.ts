import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Plugins, CameraResultType, CameraSource } from '@capacitor/core';
import { ActionSheetController, ToastController } from '@ionic/angular';
import { UsersService } from 'src/app/services/users.service';
import { AuthService } from 'src/app/services/auth.service';

const { Camera } = Plugins;

@Component({
  selector: 'app-profile-avatar',
  templateUrl: './profile-avatar.component.html',
  styleUrls: ['./profile-avatar.component.scss'],
})
export class ProfileAvatarComponent implements OnInit {
  @Output() avatarUpdated = new EventEmitter<string>();
  @Input() avatar: string;
  @Input() allowEdit: boolean;

  constructor(
    private actionSheetController: ActionSheetController,
    private toastController: ToastController,
    private authService: AuthService,
    private usersService: UsersService,
  ) { }

  ngOnInit() { }

  async selectImageSource() {
    if (!this.allowEdit) {
      return;
    }
    const buttons = [
      {
        text: 'Take Photo',
        icon: 'camera',
        handler: () => {
          this.uploadAvatar(CameraSource.Camera);
        }
      },
      {
        text: 'Choose From Your Gallery',
        icon: 'image',
        handler: () => {
          this.uploadAvatar(CameraSource.Photos);
        }
      }
    ];

    const actionSheet = await this.actionSheetController.create({
      header: 'Select Image Source',
      buttons
    });

    await actionSheet.present();
  }

  async uploadAvatar(source: CameraSource) {
    const image = await Camera.getPhoto({
      quality: 90,
      allowEditing: false,
      resultType: CameraResultType.Base64,
      source,
    });

    const blobData = this.b64toBlob(image.base64String, `image/${image.format}`);

    const formData = new FormData();
    formData.append('avatar', blobData, 'test')
    this.usersService.uploadImage(formData).then(res => {
      const idImage = res['id'];
      const newAvatar = `https://drive.google.com/uc?id=${idImage}&export=view`;
      this.avatarUpdated.emit(newAvatar);
    }).catch(async error => {
      console.error(error);
      const toast = await this.toastController.create({
        message: 'Error',
        duration: 2000
      });
      toast.present();
    });
  }

  // Helper function
  // https://stackoverflow.com/questions/16245767/creating-a-blob-from-a-base64-string-in-javascript
  b64toBlob(b64Data, contentType = '', sliceSize = 512) {
    const byteCharacters = atob(b64Data);
    const byteArrays = [];

    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);

      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }
}
