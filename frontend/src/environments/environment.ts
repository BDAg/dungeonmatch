// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiUrl: 'http://localhost:3333',
  mapbox: {
    accessToken: 'pk.eyJ1IjoiZGF2aWR0bSIsImEiOiJja2JzMjlsc2MzMDNtMnNuY24yeHg2M2l4In0.uWF56kFJ035YxdHSY8563A'
  },
  firebaseConfig: {
    apiKey: "AIzaSyAMq_PP-ufqpb6dYCvVigj2_bQR6FkGenQ",
    authDomain: "ninth-psyche-281111.firebaseapp.com",
    databaseURL: "https://ninth-psyche-281111.firebaseio.com",
    projectId: "ninth-psyche-281111",
    storageBucket: "ninth-psyche-281111.appspot.com",
    messagingSenderId: "989638862877",
    appId: "1:989638862877:web:2beb6523d956bbe2b1fef4"
  }

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
