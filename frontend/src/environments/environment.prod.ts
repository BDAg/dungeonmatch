export const environment = {
  production: true,
  apiUrl: 'http://localhost:3333',
  mapbox: {
    accessToken: 'pk.eyJ1IjoiZGF2aWR0bSIsImEiOiJja2JzMjlsc2MzMDNtMnNuY24yeHg2M2l4In0.uWF56kFJ035YxdHSY8563A'
  }
};
