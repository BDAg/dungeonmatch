# Dungeon Match Project

## A simple social media to make RPG gathering easier

## :rocket: Technologies

### Backend

- [Docker](https://www.docker.com/)
- [PostgreSQL](https://www.postgresql.org/)
- [Node](https://nodejs.org/en/)
- [TypeScript](https://www.typescriptlang.org/)
- [TypeORM](https://typeorm.io/#/)
- [Express](https://expressjs.com/)

### Frontend

- [Ionic](https://ionicframework.com/)
- [Capacitor](https://capacitor.ionicframework.com/)

## :gear: How to run

### Backend

You will need a postgres instance for database and a database named <b>dungeonmatch</b>. Docker is recomended.

Run in docker (if using): ```docker run --name postgres -e POSTGRES_PASSWORD=postgres -p 5432:5432 -d postgres```

<b>Note</b>: every time when the docker shutdowns, you will need to start the container again: ```docker start CONTAINER_ID```

#### In folder *backend* run:

To install all dependencies: ```yarn```

To config ORM and Database Migrations: ```yarn typeorm migration:run```

To run in development mode: ```yarn dev:server```

To build: ```yarn build```

### Frontend

#### In folder *frontend* run:

To install all dependencies: ```npm install```

To run in development mode: ```ionic serve```

<hr>

Watch our [demo video](https://www.youtube.com/watch?v=6K9zPOOjeDg).

## :memo: License

This project is under the MIT license. See the file [LICENSE](LICENSE) for more details.

---

### You can check more information about in [Wiki Page](https://gitlab.com/BDAg/dungeonmatch/-/wikis/home).
