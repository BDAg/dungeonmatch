import {MigrationInterface, QueryRunner, TableColumn} from "typeorm";

export default class AddRPGSystemsToUsers1590237948510 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.addColumn(
            'users',
            new TableColumn({
              name: 'rpg_systems',
              type: 'text[]',
            })
          );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropColumn('users', 'rpg_systems');
    }

}
