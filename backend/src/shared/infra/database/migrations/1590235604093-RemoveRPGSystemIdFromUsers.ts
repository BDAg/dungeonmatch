import {MigrationInterface, QueryRunner, TableColumn, TableForeignKey} from "typeorm";

export default class RemoveRPGSystemIdFromUsers1590235604093 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropForeignKey('users', 'UserRPGSystem');
        await queryRunner.dropColumn('users', 'rpg_system_id');
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.addColumn(
            'users',
            new TableColumn({
              name: 'rpg_system_id',
              type: 'uuid',
              isNullable: true,
            })
          );
      
          await queryRunner.createForeignKey(
            'users',
            new TableForeignKey({
              columnNames: ['rpg_system_id'],
              referencedColumnNames: ['id'],
              referencedTableName: 'rpg_systems',
              name: 'UserRPGSystem',
              onUpdate: 'CASCADE',
              onDelete: 'SET NULL',
            })
          );
    }

}
