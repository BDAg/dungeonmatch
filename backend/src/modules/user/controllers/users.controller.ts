import { Request, Response } from "express";
import { getRepository } from 'typeorm';

import User from '../models/User';
import CreateUserService from '../services/CreateUserService';
import UpdateUserService from '../services/UpdateUserService';
import DestroyUserService from '../services/DestroyUserService';
import AuthenticatedUserService from '../services/AuthenticatedUserService'

class UserController {
  public async create(request: Request, response: Response) {
    const {
      name,
      email,
      password,
      phone,
      user_type,
      avatar,
      location,
      rpg_systems,
    } = request.body;

    const createUserService = new CreateUserService();
    const authenticatedUserService = new AuthenticatedUserService();

    const user = await createUserService.execute({
      name,
      email,
      password,
      phone,
      user_type,
      avatar,
      location,
      rpg_systems,
    });

    const { token } = await authenticatedUserService.execute({
      email,
      password,
    });

    delete user.password;

    return response.json({ user, token });
  }

  public async show(request: Request, response: Response) {
    const { latitude, longitude, maxDistance } = request.query;

    const haversine = `(6371 * acos(cos(radians(${latitude}))
    * cos(radians(location[0]))
    * cos(radians(location[1])
    - radians(${longitude}))
    + sin(radians(${latitude}))
    * sin(radians(location[0])))) <= ${maxDistance}`;

    const userRepository = getRepository(User);

    const users = await userRepository.find({
      where: `${haversine}`,
    });

    users.map(user => delete user.password);

    return response.json(users);
  }

  public async index(request: Request, response: Response) {
    const { id } = request.params;

    const userRepository = getRepository(User);

    const user = await userRepository.findOne({
      where: { id },
    });

    delete user?.password;

    return response.json(user);
  }

  public async update(request: Request, response: Response) {
    const { id } = request.user;

    const {
      name,
      email,
      phone,
      user_type,
      avatar,
      location,
      rpg_systems,
      oldPassword,
      password,
    } = request.body;

    const updateUserService = new UpdateUserService();

    const updatedUser = await updateUserService.execute({
      id,
      name,
      email,
      phone,
      user_type,
      avatar,
      location,
      rpg_systems,
      oldPassword,
      password,
    });

    delete updatedUser?.password;

    return response.status(200).json(updatedUser);
  }

  public async destroy(request: Request, response: Response) {}
}

export default UserController;
