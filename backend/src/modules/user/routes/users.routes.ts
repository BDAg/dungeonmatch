import { Router, Request, Response } from 'express';
import { Readable } from 'stream';

import multer from 'multer';

import ensureAuthtenticated from '../../../shared/middlewares/ensureAuthenticated';
import drive from '../../../config/upload';

import UserController from '../controllers/users.controller';
import AppError from '../../../shared/errors/AppError';

const userController = new UserController();

const usersRouter = Router();

usersRouter.get('/', ensureAuthtenticated, userController.show);
usersRouter.get('/:id', ensureAuthtenticated, userController.index);
usersRouter.put('/', ensureAuthtenticated, userController.update);
usersRouter.delete('/', ensureAuthtenticated, userController.destroy);
usersRouter.post('/', userController.create);

// Matheus Sena dos Santos tem que refatorar daqui pra baixo pra ontem
function bufferToStream(binary: Buffer) {
  const readableInstanceStream = new Readable({
    read() {
      this.push(binary);
      this.push(null);
    }
  });

  return readableInstanceStream;
}

const buffer = multer.memoryStorage();
const upload = multer({ storage: buffer });

usersRouter.patch(
  '/avatar',
  ensureAuthtenticated,
  upload.single('avatar'),
  async (request: Request, response: Response) => {
    const fileMetadata = {
      parents: ['11JX7mqgxuzjGVuSnoI_DzcRWUdVtudwO'],
      name: request.file.originalname
    };

    const media = {
      mimeType: request.file.mimetype,
      body: bufferToStream(request.file.buffer)
    };

    drive.files.create({
      resource: fileMetadata,
      media: media,
      fields: 'id'
    }, (err: AppError, file: any) => {
      if (err) {
        throw new AppError('Avatar could not be saved');
      } else {
        return response.json(file.data);
      }
    });

  });

usersRouter.patch(
  '/avatar/:id',
  ensureAuthtenticated,
  upload.single('avatar'),
  async (request: Request, response: Response) => {
    const { id } = request.params;

    const media = {
      mimeType: request.file.mimetype,
      body: bufferToStream(request.file.buffer)
    };

    drive.files.update({
      media: media,
      fileId: id,
      fields: 'id'
    }, (err: AppError, file: any) => {
      if (err) {
        throw new AppError('Avatar could not be saved');
      } else {
        return response.json(file.data);
      }
    });

  });

export default usersRouter;
