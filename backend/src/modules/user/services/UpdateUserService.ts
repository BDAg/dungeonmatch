import { getRepository } from "typeorm";
import { compare, hash } from 'bcryptjs';

import AppError from "../../../shared/errors/AppError";
import User from "../models/User";

interface Request {
  id: string;
  name: string;
  email: string;
  phone: string;
  user_type: 'player' | 'master' | 'both';
  avatar: string;
  rpg_systems: string[];
  location?: string;
  password?: string;
  oldPassword?: string;
}

class UpdateUserService {
  public async execute({
    id,
    name,
    email,
    phone,
    user_type,
    rpg_systems,
    avatar,
    location,
    password,
    oldPassword
  }: Request): Promise<User> {
    const userRepository = await getRepository(User);

    const user = await userRepository.findOne({
      where: { id },
    });

    if (!user) {
      throw new AppError('User not found');
    }

    const userWithUpdatedEmail = await userRepository.findOne({
      where: { email }
    });

    if (userWithUpdatedEmail && userWithUpdatedEmail.id !== id) {
      throw new AppError('Email is already taken');
    }

    Object.assign(user, {
      name,
      email,
      phone,
      user_type,
      avatar,
      rpg_systems,
      location,
    });

    if (password && !oldPassword) {
      throw new AppError('You need to send the old password to create a new');
    }

    if (password && oldPassword) {
      const validOldPassword = await compare(oldPassword, user.password);

      if (!validOldPassword) {
        throw new AppError('Old password is wrong')
      }

      user.password = await hash(password, 8);
    }

    return userRepository.save(user);
  }
}

export default UpdateUserService;
