import { getRepository } from 'typeorm';
import { hash } from 'bcryptjs';

import AppError from '../../../shared/errors/AppError';

import User from '../models/User';

interface Request {
  name: string;
  email: string;
  password: string;
  phone: string;
  user_type: 'player' | 'master' | 'both';
  avatar: string;
  location: number[];
  rpg_systems: string[];
}

class CreateUserService {
  public async execute({ 
    name, 
    email, 
    password, 
    phone, 
    user_type,
    avatar,
    location, 
    rpg_systems
  }: Request): Promise<User> {
    const userRepository = getRepository(User);

    const checkUserExists = await userRepository.findOne({
      where: { email },
    });

    if (checkUserExists) {      
      throw new AppError('Email address already used');
    }

    const hashedPassword = await hash(password, 8);

    const user = userRepository.create({
      name,
      email,
      password: hashedPassword,
      phone,
      avatar,
      location,
      user_type,
      rpg_systems,
    });

    await userRepository.save(user);

    return user;
  }
}

export default CreateUserService;